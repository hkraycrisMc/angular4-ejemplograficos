import { Component, ElementRef, ViewChild  } from '@angular/core';
import { WeatherService } from './weather.service';
import { Chart } from 'chart.js';
import 'rxjs/add/operator/toPromise';
import { Http, Headers } from '@angular/http';
import { saveAs } from 'file-saver/FileSaver';
import 'jspdf';
declare let jsPDF;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  // get the element with the #chessCanvas on it
  @ViewChild("canvas") chessCanvas: ElementRef;

  chart = []; // This will hold our chart info

  constructor(private elementRef: ElementRef) { }

  ngOnInit() {  }

  ngAfterViewInit() {
    let context: CanvasRenderingContext2D = this.chessCanvas.nativeElement.getContext("2d");

    console.log(context);
    this.chart = new Chart(context, {
      type: 'line',
      data: {
        labels: [1500, 1600, 1700, 1750, 1800, 1850, 1900, 1950, 1999, 2050],
        datasets: [{
          data: [86, 114, 106, 106, 107, 111, 133, 221, 783, 2478],
          label: "Africa",
          borderColor: "#3e95cd",
          fill: false
        }, {
          data: [282, 350, 411, 502, 635, 809, 947, 1402, 3700, 5267],
          label: "Asia",
          borderColor: "#8e5ea2",
          fill: false
        }
        ]
      },
      options: {
        title: {
          display: true,
          text: 'World population per region (in millions)'
        }
      }
    });

  }
   download() {
    let context: HTMLCanvasElement = this.chessCanvas.nativeElement;
    context.toBlob(function (blob) {
      saveAs(blob, "chart_1.png");
    });
  }
}
